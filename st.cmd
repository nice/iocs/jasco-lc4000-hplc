require essioc
require jasco 

epicsEnvSet("STREAM_PROTOCOL_PATH", "$(jasco_DIR)/db/")
epicsEnvSet("HOSTNAME", "jasco-lc4000-hplc.cslab.esss.lu.se")
epicsEnvSet("PORT", "4001")
epicsEnvSet("DEVICE", "MOXA")
epicsEnvSet("P", "SE-SEE:")
epicsEnvSet("R", "SE-JASCO-001:")

iocshLoad("$(jasco_DIR)jasco4180.iocsh", "P=$(P), R=$(R), IPADDR=$(HOSTNAME), IPPORT=$(PORT)")
iocshLoad("$(essioc_DIR)/common_config.iocsh")

seq detect_jasco_error_state, "SETPOINT=$(P)COMP:A:SP,READBACK=$(P)COMP:A,STATUS=$(P)STATUS,PUMPRUN=$(P)START:SP,TIMERUN=$(P)PUMP_FOR_TIME:SP,PUMPSTOP=$(P)STOP:SP,ERROR=$(P)ERROR:COMP,TOLERANCE=$(P)ERROR:TOL,DELAY=$(P)ERROR:DELAY,RESET=$(P)RESET:SP"
