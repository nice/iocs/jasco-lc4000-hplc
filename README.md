# e3-ioc-jasco-LC400-HPLC

Jasco LC-400 HPLC IOC

---

## IOC template

This project was generated with the [E3 cookiecutter IOC template](https://gitlab.esss.lu.se/ics-cookiecutter/cookiecutter-e3-ioc).

This README.md should be updated as part of creation and should add complementary information about the IOC in question (hardware controlled, addresses, etc.).


Source code of the module: https://github.com/ISISComputingGroup/EPICS-ioc/tree/master/JSCO4180
